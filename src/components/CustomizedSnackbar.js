import React from "react";
import {Snackbar} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";

// local
import {useActions} from "../hooks/useActions";

export const CustomizedSnackbars = ({snackbar: {open, message, severity}}) => {
    const {closeSnackbar} = useActions();

    const handleClose = (event, reason) => {
        closeSnackbar(reason);
    };

    return (
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <MuiAlert
                elevation={6}
                variant="filled"
                onClose={handleClose}
                severity={severity}
            >
                {message}
            </MuiAlert>
        </Snackbar>
    );
}
