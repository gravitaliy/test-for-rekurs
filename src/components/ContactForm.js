import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {TextField, MenuItem, Box, IconButton, Button} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {AddCircle, RemoveCircle, Send} from "@material-ui/icons";

// local
import {useActions} from "../hooks/useActions";
import {options} from "../assets/enums/options";
import {LoaderBackdrop} from "./LoaderBackdrop";
import {CustomizedSnackbars} from "./CustomizedSnackbar";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        "& .MuiTextField-root": {
            margin: theme.spacing(1),
            width: "25ch",
        },
    },
    row: {
        display: "flex",
        alignItems: "center",
    },
    button: {
        margin: theme.spacing(1),
    },
}));

export const ContactForm = () => {
    const {
        loadForm,
        submitForm,
        changeInput,
        changeSelect,
        addRow,
        deleteRow
    } = useActions();

    const {inputs, loading, snackbar} = useSelector(state => state.form);

    const classes = useStyles();

    useEffect(() => {
        loadForm();
    }, []);

    const handleAddRow = id => () => {
        addRow(id);
    }

    const handleDeleteRow = id => () => {
        deleteRow(id);
    }

    const handleChangeSelect = ({target: {name, value}}) => {
        changeSelect(name, value);
    }

    const handleChangeInput = id => ({target: {value}}) => {
        changeInput(value, id);
    }

    const handleSubmit = e => {
        e.preventDefault();
        submitForm();
    }

    return (
        <>
            {loading && <LoaderBackdrop loading={loading} />}
            {snackbar && <CustomizedSnackbars snackbar={snackbar} />}
            <form
                className={classes.root}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
            >
                {inputs.map(({id, type, value}) => (
                    <Box className={classes.row} key={id}>
                        <TextField
                            id="standard-select-currency"
                            name={`${id}`}
                            select
                            value={type}
                            onChange={handleChangeSelect}
                        >
                            {options.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                        <TextField
                            value={value}
                            id="standard-basic"
                            onChange={handleChangeInput(id)}
                        />
                        <IconButton
                            color="primary"
                            aria-label="add"
                            onClick={handleAddRow(id)}
                        >
                            <AddCircle/>
                        </IconButton>
                        <IconButton
                            color="secondary"
                            aria-label="delete"
                            onClick={handleDeleteRow(id)}
                            disabled={inputs.length === 1 && !value}
                        >
                            <RemoveCircle/>
                        </IconButton>
                    </Box>
                ))}
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    className={classes.button}
                    endIcon={<Send/>}
                >
                    Send
                </Button>
            </form>
        </>
    );
};
