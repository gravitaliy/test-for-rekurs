import {formActionTypes} from "../../assets/enums/formActionTypes";
import {options} from "../../assets/enums/options";

export const rowObj = ({id = 0, type = options[0].value, value = ""} = {}) => {
    return {id, type, value}
}

const initialState = {
    inputs: [rowObj()],
    loading: false,
    snackbar: {
        open: false,
        message: null,
        severity: null,
    },
}

export const formReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case formActionTypes.LOAD_FORM:
            return {
                ...state,
                inputs: payload,
            };

        case formActionTypes.SUBMIT_FORM:
            return {
                ...state,
                loading: true,
            };

        case formActionTypes.EMPTY_FORM:
            return {
                ...state,
                snackbar: {
                    open: true,
                    message: payload.message,
                    severity: payload.severity,
                },
            };

        case formActionTypes.SUBMIT_FORM_SUCCESS:
            return {
                ...state,
                loading: false,
                inputs: initialState.inputs,
                snackbar: {
                    open: true,
                    message: payload.message,
                    severity: payload.severity,
                },
            };

        case formActionTypes.SUBMIT_FORM_ERROR:
            return {
                ...state,
                loading: false,
                snackbar: {
                    open: true,
                    message: payload.message,
                    severity: payload.severity,
                },
            };

        case formActionTypes.CHANGE_INPUT:
            return {
                ...state,
                inputs: payload,
            };

        case formActionTypes.CHANGE_SELECT:
            return {
                ...state,
                inputs: payload,
            };

        case formActionTypes.ADD_ROW:
            return {
                ...state,
                inputs: payload,
            };

        case formActionTypes.DELETE_ROW:
            return {
                ...state,
                inputs: payload,
            };

        case formActionTypes.CLOSE_SNACKBAR:
            return {
                ...state,
                snackbar: {
                    ...state.snackbar,
                    open: false,
                },
            };

        default:
            return state;
    }
}
