import {combineReducers} from "redux";

// local
import {formReducer} from "./userReducer";

export const rootReducer = combineReducers({
    form: formReducer,
});
