import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

// local
import {rootReducer} from "./reducers";
import {LOCAL_STORAGE, LocalStorageHelper} from "../assets/localStorage";

export const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

store.subscribe(() => {
    const inputs = store.getState().form.inputs;
    const noEmptyArr = inputs.filter(item => !!item.value);

    LocalStorageHelper.setItem(LOCAL_STORAGE.FORM_INPUTS, noEmptyArr);
});
