import * as UserActionCreators from "./userActions";

const exportedObject = {
    ...UserActionCreators
};

export default exportedObject;
