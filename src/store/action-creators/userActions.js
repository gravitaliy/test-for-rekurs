import {formActionTypes} from "../../assets/enums/formActionTypes"
import {rowObj} from "../reducers/userReducer";
import {LOCAL_STORAGE, LocalStorageHelper} from "../../assets/localStorage";
import {store} from "../index";


const getInputs = () => store.getState().form.inputs;


export const loadForm = () => {
    return dispatch => {
        const getStorage = LocalStorageHelper.getItem(LOCAL_STORAGE.FORM_INPUTS);

        if (getStorage && getStorage.length !== 0) {
            dispatch({type: formActionTypes.LOAD_FORM, payload: getStorage});
        }
    }
}

export const submitForm = () => {
    return async dispatch => {
        const noEmptyArr = getInputs().filter(item => !!item.value);

        if (noEmptyArr.length === 0) {
            const payload = {
                message: "Нужно заполнить хотя бы одно поле",
                severity: "warning",
            };

            dispatch({type: formActionTypes.EMPTY_FORM, payload});
            return;
        }

        dispatch({type: formActionTypes.SUBMIT_FORM});

        setTimeout(() => {
            try {
                if (Math.random() < 0.3) {
                    throw new Error("Произошла ошибка при отправке формы");
                }

                console.log("getFormValues", getFormValues(noEmptyArr));
                console.log("convertArrayToObject", convertArrayToObject(getFormValues(noEmptyArr)));

                const payload = {
                    message: "Форма успешно отправлена",
                    severity: "success",
                };

                dispatch({type: formActionTypes.SUBMIT_FORM_SUCCESS, payload});
            } catch ({message}) {
                const payload = {
                    message,
                    severity: "error",
                };

                dispatch({type: formActionTypes.SUBMIT_FORM_ERROR, payload});
            }
        }, 1000);
    }
}


export const changeInput = (value, id) => {
    return dispatch => {
        const payload = getInputs().map(item => {
            if (item.id === id) return rowObj({...item, value});
            return item;
        });

        dispatch({type: formActionTypes.CHANGE_INPUT, payload});
    }
}


export const changeSelect = (id, type) => {
    return dispatch => {
        const payload = getInputs().map(item => {
            if (item.id === +id) return rowObj({...item, type});
            return item;
        });

        dispatch({type: formActionTypes.CHANGE_SELECT, payload});
    }
}


export const addRow = id => {
    return dispatch => {
        const inputs = getInputs();
        const newId = Math.max(...inputs.map(item => item.id)) + 1;
        const index = inputs.findIndex(i => i.id === id);

        const payload = [
            ...inputs.slice(0, index + 1),
            rowObj({id: newId}),
            ...inputs.slice(index + 1),
        ];

        dispatch({type: formActionTypes.ADD_ROW, payload});
    }
}


export const deleteRow = id => {
    return dispatch => {
        const inputs = getInputs();
        let payload = inputs.filter(item => item.id !== id);

        if (inputs.length === 1) {
            payload = [rowObj({id: inputs[0].id, value: ""})]
        }

        dispatch({type: formActionTypes.DELETE_ROW, payload});
    }
}


export const closeSnackbar = reason => {
    return dispatch => {
        if (reason === "clickaway") return;

        dispatch({type: formActionTypes.CLOSE_SNACKBAR});
    }
}


export const getFormValues = form => {
    const typesArr = [];
    const valuesArr = [];

    form.forEach(({type, value}) => {
        typesArr.push(type);
        valuesArr.push(value);
    });

    return {
        type: typesArr,
        value: valuesArr,
    }
}


export const convertArrayToObject = ({type, value}) => {
    return type.map((item, idx) => (
        {
            type: item,
            value: value[idx],
        }
    ));
}
