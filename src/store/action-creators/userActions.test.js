import {convertArrayToObject, getFormValues} from "./userActions";

describe("Test convert methods", () => {
    let data = [];

    beforeEach(() => {
        data = [
            { id: 0, type: "email", value: "111" },
            { id: 1, type: "phone", value: "222" },
            { id: 2, type: "link", value: "333" },
        ];
    });

    it("should getFormValues", () => {
        const res = {
            type: ["email", "phone", "link"],
            value: ["111", "222", "333"],
        };
        expect(getFormValues(data)).toEqual(res);
    });

    it("should convertArrayToObject", () => {
        const res = [
            { type: "email", value: "111" },
            { type: "phone", value: "222" },
            { type: "link", value: "333" },
        ];
        expect(convertArrayToObject(getFormValues(data))).toEqual(res);
    });
});
