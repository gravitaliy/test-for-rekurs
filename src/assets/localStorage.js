export const LocalStorageHelper = {
    getItem(key) {
        return JSON.parse(localStorage.getItem(key));
    },
    setItem(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    },
    delete(key) {
        delete localStorage[key];
    },
};

export const LOCAL_STORAGE = {
    FORM_INPUTS: "form_inputs",
};
