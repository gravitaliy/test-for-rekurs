export const options = [
    {
        value: "email",
        label: "Email",
    },
    {
        value: "phone",
        label: "Phone",
    },
    {
        value: "link",
        label: "Link",
    },
];
