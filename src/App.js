import React from "react";
import {Provider} from "react-redux";
import "@fontsource/roboto";

// local
import {store} from "./store";
import {ContactForm} from "./components/ContactForm";

export const App = () => {
    return (
        <Provider store={store}>
            <ContactForm/>
        </Provider>
    );
}
